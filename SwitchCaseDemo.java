package com.two;

import java.util.Scanner;

/**
 * @author Lathusan Thurairajah
 * @email lathusanthurairajah@gmail.com
 * @version 1.0
 * @Jan 21, 2021
 **/

public class SwitchCaseDemo {

	public static void main(String[] args) {

		Cases cases = new Cases();
		Scanner scan = new Scanner(System.in);

		System.out.println("Enter the box size : \n1. 3\n2. 4\n3. 5\n4. 6");
		int input = scan.nextInt();

		switch (input) {
		case 3:
			cases.caseOne();
			break;
		case 4:
			cases.caseTwo();
			break;
		case 5:
			cases.caseThree();
			break;
		case 6:
			cases.caseFour();
			break;
		default:
			System.out.println("Please Select a Size.");
			break;
		}

	}

}
