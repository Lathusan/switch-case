package com.two;

/**
 * @author Lathusan Thurairajah
 * @email lathusanthurairajah@gmail.com
 * @version 1.0
 * @Jan 21, 2021
 **/

public class Cases {

	public void caseOne() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print("* ");
			}
			System.out.println("");
		}
	}

	public void caseTwo() {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				System.out.print("* ");
			}
			System.out.println("");
		}
	}

	public void caseThree() {
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				System.out.print("* ");
			}
			System.out.println("");
		}
	}

	public void caseFour() {
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 6; j++) {
				System.out.print("* ");
			}
			System.out.println("");
		}
	}

}
